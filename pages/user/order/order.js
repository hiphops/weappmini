// pages/user/order/order.js
const fetch = require('../../../utils/fetch')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseurl: app.config.baseUrl, // 图片公共路径
    orderList: [], // 订单列表
  },

  /* 获取订单列表信息 */
  async getOrder() {
    let {data} = await fetch(`apis/my_orders`, {uid: wx.getStorageSync('userInfo').id}, 'get', {token: wx.getStorageSync('userInfo').token})
    // console.log(data);
    if(data.msg.length > 0) {
      this.setData({
      orderList: data.msg
    })
    }
    
  },

    /* 获取详情 */
    async goDetail(e) {
      wx.navigateTo({
        url: '/pages/user/orderdetail/orderdetail?id=' + e.target.dataset.id,
      })
    },

      /* 删除订单列表信息 */
  async delOrder(e) {
    let {data} = await fetch(`apis/del_orders`, {id: e.target.dataset.id}, 'get', {token: wx.getStorageSync('userInfo').token})
    // console.log(data);
    if(data.msg > 0) {
      this.getOrder()
    }
  },

    /* 确认收货 */
    sureOrder() {
      wx.showToast({
        title: `这是个假的，不要信以为真`,
        icon: 'none',
      });
    },

  // async getOrder2() {
  //   let {data} = await fetch(`apis/my_order_details`, {id: 6})
  //   console.log(data);
  // },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getOrder()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})