// pages/msg/msg.js
const fetch = require('../../utils/fetch')
Page({

  /**
   * 页面的初始数据
   * 这里的买的鸡爪非常好吃，大家都喜欢吃，今天要多吃点
   */
  data: {
    msg: '',
    list: [],
    uid: 0,
    page: 1,
    pagesize:6,
    total: 0, // 消息总数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getMsg()
    if(wx.getStorageSync('userInfo')) {
      this.setData({uid: wx.getStorageSync('userInfo').id})
    }
  },

  /* 获取消息 */
  async getMsg(callback) {
    let {data} = await fetch(`apis/getmessage`, {
        page: this.data.page,
        pagesize: this.data.pagesize
    })
    // console.log(data);
    if(data) {
        callback && callback()
        const msgs = this.data.list.concat(data.msg.data)
        this.setData({list: msgs, total: data.msg.total})
    }
    
  },

   /* 删除消息 */
   async delMsg(e) {
    let {data} = await fetch(`apis/delmessage`, {id: e.target.dataset.id})
    console.log(data);
    if(data.msg == 1) {
      this.getMsg()
    }
  },

  /* 发送消息 */
  async send() {
    if(!wx.getStorageSync('userInfo')) {
      wx.redirectTo({
        url: '/pages/user/login/login'//页面路径
      })
      return
    }
    if(this.data.msg.trim() == '') {
      wx.showToast({
        title: `消息不能为空`,
        icon: 'none',
      });
      return
    }
    // 开始发送请求
    let {data} = await fetch(`apis/addmessage`, {uid: wx.getStorageSync('userInfo').id, author: wx.getStorageSync('userInfo').nickname, avatar: wx.getStorageSync('userInfo').avatar, content: this.data.msg}, 'post', {token: wx.getStorageSync('userInfo').token})
    console.log(data);
    if(data.msg == 1) {
      this.setData({msg: ''})
      this.getMsg()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if(wx.getStorageSync('userInfo')) {
      this.setData({uid: wx.getStorageSync('userInfo').id})
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.setData({
        page: 1,
        list: []
    })
    this.getMsg(function() {
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
    })    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    if(this.data.list.length == this.data.total) {
        wx.showToast({
          title: '没有更多数据了',
        })
        return
    }
    let num = this.data.page + 1
    this.setData({
        page: num
    })
    this.getMsg()    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})