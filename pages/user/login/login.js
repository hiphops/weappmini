// pages/user/login/login.js
const fetch = require('../../../utils/fetch')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    uname: '', // 用户名
    upwd: '', // 密码
    unamer: '', // 用户名
    upwdr: '', // 密码
    upwdr2: '', // 确认密码
    tel: '', // 电话
    email: '', // 邮箱
    erruname: '', // 错误提示
    flagruname: false, // 注册用户名焦点
  },
  // 检测用户不能为空
  onuname(e) {
    if(this.data.uname == '') {
      this.setData({erruname: '用户名不能为空'})
      console.log(e);
    } else {
      this.setData({erruname: ''})
    }
  },
  /* 用户登录 */
  async login() {
    let {data} = await fetch(`user/login`, {account: this.data.uname, password: this.data.upwd})
    console.log(data);
    if(data.msg == '登录成功') {
      wx.setStorageSync('userInfo', data.data.userinfo)
      wx.switchTab({
        url: '../../profile/profile' // 登录成功跳转到个人中心
      })
    } else {
       wx.showToast({
          title: `登录失败，请检查用户名或密码`,
          icon: 'none',
        });
    }
  },
  /* 用户注册 */
  async reg() {
    if(this.data.unamer.trim() == '') {
      wx.showToast({
        title: `用户名不能为空`,
        icon: 'none',
      });
      this.setData({flagruname: true})
      return
    }
    if(this.data.upwdr.trim() == '') {
      wx.showToast({
        title: `密码不能为空`,
        icon: 'none',
      });
      return
    }
    if(this.data.tel.trim() == '') {
      wx.showToast({
        title: `手机号不能为空`,
        icon: 'none',
      });
      return
    }
    if(this.data.email.trim() == '') {
      wx.showToast({
        title: `邮箱不能为空`,
        icon: 'none',
      });
      return
    }
    if(this.data.upwdr != this.data.upwdr2) {
      wx.showToast({
        title: `密码与确认密码不一致`,
        icon: 'none',
      });
      return
    }
    let res = await fetch(`user/register`, {username: this.data.unamer, password: this.data.upwdr, email: this.data.email, mobile: this.data.tel})
    // console.log(res);
    if(res.data.msg == '注册成功') {
      // this.onLoad()
      wx.redirectTo({
        url: '/pages/user/login/login'//页面路径
      })
    } else {
       wx.showToast({
          title: `注册失败，${res.data.msg}`,
          icon: 'none',
        });
    }
  },


   /* tab切换效果 */
  onChange(event) {
    // 弹出提示
    // wx.showToast({
    //   title: `切换到标签 ${event.detail.name}`,
    //   icon: 'none',
    // });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})