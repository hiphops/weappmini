// pages/user/editinfo/editinfo.js
const fetch = require('../../../utils/fetch')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    uname: '', // 用户名
    nickname: '', // 呢称
    bio: '', // 简介
    avatar: '', // 图像
    img: '', // 上传后图片路径
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    if(!wx.getStorageSync('userInfo')) {
      wx.redirectTo({
        url: '/pages/user/login/login'//页面路径
      })
    }else {
      this.setData({
        uname: wx.getStorageSync('userInfo').username,
        nickname: wx.getStorageSync('userInfo').nickname,
        avatar: wx.getStorageSync('userInfo').avatar
      })
    }
  },

  /* upload修改图片，上传图片 */
  upload() {
    var that = this
    wx.chooseImage({
      count: 1, // 最多可以选择的图片张数，默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths;
        console.log(tempFilePaths);
        wx.getFileSystemManager().readFile({
          filePath: tempFilePaths[0],
          encoding: 'base64',
          success: function (res) {
            console.log('data:image/png;base64,' + res.data);
            // 将base64格式的图片上传到服务器
            that.setData({
              avatar: 'data:image/png;base64,' + res.data
            })
          }
        })
      }
    })
  },

  /* 提交修改信息 */
  async submitEdit() {
    let {data} = await fetch(`user/profile`, {username: this.data.uname, nickname: this.data.nickname, bio: this.data.bio, avatar: this.data.avatar})
    console.log(data);
    if(data.code == 1) {
      var obj = wx.getStorageSync('userInfo')
      obj.username = this.data.uname
      obj.nickname = this.data.nickname
      obj.avatar = this.data.avatar
      wx.setStorageSync('userInfo', obj)
      wx.switchTab({
        url: '../../profile/profile' // 登录成功跳转到个人中心
      })
    }
  },

  /* 获取微信图像 */
  onChooseAvatar(e) {
      console.log(111);
    var that = this
    const { avatarUrl } = e.detail 
    console.log('微信图像', avatarUrl);
    wx.uploadFile({
      url: 'http://192.168.8.3:8088/zgf/public/index.php/api/common/upload', //仅为示例，非真实的接口地址
      filePath: avatarUrl,
      name: 'file',
      header: {
        token: wx.getStorageSync('userInfo').token
      },
      formData: {
        'file': 'test'
      },
      success (res){
        console.log('上传后',res);
        var data = JSON.parse(res.data)
        if(data.msg == '上传成功') {
          var url = app.config.baseUrl +  data.data.url
          console.log(url);        
          that.setData({
            avatar: url,
          })
        }
        
        
        //do something
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if(!wx.getStorageSync('userInfo')) {
      wx.redirectTo({
        url: '/pages/user/login/login'//页面路径
      })
    }else {
      this.setData({
        uname: wx.getStorageSync('userInfo').username,
        nickname: wx.getStorageSync('userInfo').nickname,
        avatar: wx.getStorageSync('userInfo').avatar
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})