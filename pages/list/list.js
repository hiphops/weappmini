const fetch = require('../../utils/fetch')
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    catid: 0,
    category: null,
    shops: [],
    pageIndex: 0,
    pageSize: 7,
    totalCount: 0,
    hasMore: true,
    baseurl: app.config.baseUrl,
    searchText: '',
  },

  loadMore (pid) {
    let { pageIndex, pageSize, searchText } = this.data

    const params = { page: ++pageIndex, pagesize: pageSize, pid:pid }
    if (searchText) {
      // 当有搜索内容时，执行搜索接口
      params.like = searchText
      wx.setNavigationBarTitle({ title: searchText })
      return fetch(`apis/yhsearch_list`, params)
      .then(res => {
        console.log(res);
        const totalCount = parseInt(res.data.msg.total)
        const hasMore = ++this.data.pageIndex * this.data.pageSize < totalCount
        console.log(hasMore,totalCount,this.data.pageIndex);
        const shops = this.data.shops.concat(res.data.msg.data)
        this.setData({ shops, totalCount, pageIndex, hasMore })
      })
    } else {
      // 没有搜索参数时则调用分类产品
      return fetch(`apis/yhprotype_list`, params)
      .then(res => {
        console.log(res);
        const totalCount = parseInt(res.data.msg.total)
        const hasMore = ++this.data.pageIndex * this.data.pageSize < totalCount
        console.log(hasMore,totalCount,this.data.pageIndex);
        const shops = this.data.shops.concat(res.data.msg.data)
        this.setData({ shops, totalCount, pageIndex, hasMore })
      })
    }

    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad (options) {
    this.setData({catid: options.cat})
    wx.setNavigationBarTitle({ title: options.catname })
    this.loadMore(options.cat)
    // fetch(`/categories/${options.cat}`)
    //   .then(res => {
    //     this.setData({ category: res.data })
    //     this.loadMore()
    //   })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh () {
    this.setData({ shops: [], pageIndex: 0, hasMore: true })
    this.loadMore(this.data.catid).then(() => wx.stopPullDownRefresh())
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom () {
    // TODO：节流
    this.loadMore(this.data.catid)
  },

  searchHandle () {
    // console.log(this.data.searchText)
    this.setData({ shops: [], pageIndex: 0, hasMore: true })
    this.loadMore()
  },

  showSearchHandle () {
    this.setData({ searchShowed: true })
  },
  hideSearchHandle () {
    this.setData({ searchText: '', searchShowed: false })
  },
  clearSearchHandle () {
    this.setData({ searchText: '' })
  },
  searchChangeHandle (e) {
    this.setData({ searchText: e.detail.value })
  }
})
