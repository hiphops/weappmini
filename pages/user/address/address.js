// pages/user/address/address.js
const fetch = require('../../../utils/fetch')
var one = [
  { id: 111, 'name': '湖南' },
  { id: 222, 'name': '湖北' },
  ]
  var two = {
  '湖南': [
  { id: 11100, 'name': '长沙' },
  { id: 22201, 'name': '邵阳' },
  ],
  '湖北': [
  { id: 11102, 'name': '武汉' },
  { id: 22203, 'name': '黄冈' },
  ]
  }
  var three = {
  '长沙': [
  { id: 1110000, 'name': '芙蓉区' },
  { id: 2220101, 'name': '天心区' },
  ],
  '邵阳': [
  { id: 1110102, 'name': '大祥区' },
  { id: 2220103, 'name': '双清区' },
  { id: 3330104, 'name': '北塔区' },
  ],
  '武汉': [
  { id: 1110205, 'name': '青山区' },
  { id: 2220206, 'name': '汉口区' },
  { id: 3330207, 'name': '武昌区' },
  ],
  '黄冈': [
  { id: 1110308, 'name': '蕲春县' },
  { id: 2220309, 'name': '浠水县' },
  { id: 3330310, 'name': '黄梅县' },
  ],
  }
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // region: ['广东省', '广州市', '海珠区'],
    // customItem: '全部',
    list: [],
    show: false, // 隐藏添加弹出层
    show2: false, // 隐藏弹出层三级联动
    btntxt: '保存', // 添加地址按钮默认为保存，点击修改则为修改
    person: '', // 收货人
    tel: '', // 收货人电话
    diqu: '', // 收货地区
    pro: '', // 省
    city: '', // 市
    area: '', // 区
    detail: '', // 详细地址
    editid: 0, // 修改收货地址id
    columns: [
      {
      values: one,                              //可以是数组，或者对象数组
      className: 'names'                      //选择器的第⼀列
      },
      {
      values: two[one[0].name],                //默认选中two对象中的第⼀项
      className: 'column2',                    //选择器的第⼆列
      },
      {
      values: three[two[one[0].name][0].name],  //默认选中three对象中的第⼀项        className: 'column3',                    //选择器的第三列
      }
      ],
      one: [],
      two: {},
      three: {},
      txt: '', // 判断是否从订单选择地址进来
  },

  /* 获取三级联动数据 */
  async getPro() {
    let res1 = await fetch(`apis/getPro`)
    let res2 = await fetch(`apis/getCity`)
    let res3 = await fetch(`apis/getArea`)
    console.log(res1);
    var one = res1.data.msg
    var two = res2.data.msg
    var three = res3.data.msg
    this.setData({
      one: one,
      two: two,
      three: three,
      // list: data.msg
      columns:[
        {
        values: one,                              //可以是数组，或者对象数组
        className: 'names'                      //选择器的第⼀列
        },
        {
        values: two[one[0].pid],                //默认选中two对象中的第⼀项
        className: 'column2',                    //选择器的第⼆列
        },
        {
        values: three[two[one[0].pid][0].pid],  //默认选中three对象中的第⼀项        className: 'column3',                    //选择器的第三列
        }
        ]
    })
  },
//   bindRegionChange: function (e) {
//     console.log('picker发送选择改变，携带值为', e.detail.value)
//     this.setData({
//       region: e.detail.value
//     })
//   },
/* 获取收货地址列表 */
  async getAddress() {
    let {data} = await fetch(`apis/getaddress`, {uid: wx.getStorageSync('userInfo').id}, 'get', {token: wx.getStorageSync('userInfo').token})
    console.log(data);
    this.setData({
      list: data.msg
    })
  },

    /* 选中设置默认地址 */
  async setAddress(e) {
    // console.log(666, e);
    let {data} = await fetch(`apis/setaddress`, {uid: wx.getStorageSync('userInfo').id, id:e.detail.value}, 'get', {token: wx.getStorageSync('userInfo').token})
    if(data.msg != 1) {
      wx.showToast({
        title: `设置默认地址失败`,
        icon: 'none',
      });
    }
  },

  /* 点击修改时弹出修改表单，并把保存按钮文本改为修改 */
  edit(e) {    
    console.log(e.target.dataset.index);
    var obj = this.data.list[e.target.dataset.index]
    this.setData({
      btntxt: '修改',
      show: true,
      person: obj.person,
      tel: obj.tel,
      pro: obj.pro,
      city: obj.city,
      area: obj.area,
      diqu: obj.pro + ' ' + obj.city + ' ' + obj.area,
      detail: obj.detail,
      editid: e.target.dataset.id
    })
  },

      /* 添加新的收货地址 */
      async saveAddress(e) {
        if(this.data.btntxt == '保存') {
          let {data} = await fetch(`apis/addaddress`, {uid: wx.getStorageSync('userInfo').id, person: this.data.person, tel: this.data.tel, pro: this.data.pro, city: this.data.city, area: this.data.area, detail: this.data.detail}, 'post', {token: wx.getStorageSync('userInfo').token})
          console.log(data);
          if(data.msg == 1) {
            this.setData({
              show: false
            })
            this.getAddress()
          }
        } else {
          // 修改收货地址
          let {data} = await fetch(`apis/editaddress`, {id: this.data.editid, uid: wx.getStorageSync('userInfo').id, person: this.data.person, tel: this.data.tel, pro: this.data.pro, city: this.data.city, area: this.data.area, detail: this.data.detail}, 'post', {token: wx.getStorageSync('userInfo').token})
          console.log(data);
          if(data.msg == 1) {
            this.setData({
              show: false,
              person: '',
              tel: '',
              pro: '',
              city: '',
              area: '',
              detail: '',
              diqu: '',
              btntxt: '保存'
            })
            this.getAddress()
          }
        }
        
      },

  /* 删除收货地址 */
  async delAddress(e) {
    // console.log(666, e);
    let {data} = await fetch(`apis/deladdress`, { id:e.target.dataset.id}, 'get', {token: wx.getStorageSync('userInfo').token})
    if(data.msg == 1) {
      this.getAddress()
    } else {
      wx.showToast({
        title: `删除失败`,
        icon: 'none',
      });
    }
  },

  // 新增收货地址弹出层
  add() {
    this.setData({
      show: true,
      person: '',
      tel: '',
      pro: '',
      city: '',
      area: '',
      detail: '',
      diqu: '',
      btntxt: '保存'
    })
  },
  onClose() {
    this.setData({ show: false });
  },
  setAdd() {
    this.setData({show2: true})
  },

  /* 选择地址跳转到订单页 */
  toOrder(e) {
    if(this.data.txt != '') {
      console.log(e);
      wx.navigateTo({
        url: '/pages/user/cartorder/cartorder?ad=' + JSON.stringify(e.currentTarget.dataset.obj),
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getAddress()
    this.getPro()
    if(options.txt) {
      this.setData({ txt: options.txt })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },



  onConfirm(event) {
    let value = event.detail.value
    //将选中的⽂字和对应的id拿出来使⽤
      this.setData({
        pro: value[0].name,
        city: value[1].name,
        area: value[2].name,
        diqu: value[0].name + ' ' + value[1].name + ' ' + value[2].name,
        show2: false
      })
    },
    onChange(event) {
    let picker = event.detail.picker
    let value = event.detail.value
    let index = event.detail.index
    //在change 第⼀列的时候，动态更改第⼆列的数据
    //setColumnValues是vant⾃带的实例⽅法
    //第⼀个参数是列数，从0开始；第⼆个参数是第⼆列应该显⽰的数据
    picker.setColumnValues(1, this.data.two[value[0].pid])
    //此处vant-picker有⼀个bug，当只滑动第⼀级时，返回的value数据是错误的，需要我们⾃⼰根据第⼆
    // 级计算，去获取第三级数据
    if (index == 0) {
    picker.setColumnValues(2, this.data.three[(this.data.two[value[0].pid][0]).pid])
    } else {
    picker.setColumnValues(2, this.data.three[value[1].pid])
    }
    },
    onCancel() {
      this.setData({show2: false})
    }






})