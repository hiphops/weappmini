// pages/user/orderdetail/orderdetail.js
const fetch = require('../../../utils/fetch')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseurl: app.config.baseUrl, // 图片公共路径
    orderList: [], // 订单列表
  },

  async getOrder(id) {
    let {data} = await fetch(`apis/my_order_details`, {id: id})
    // console.log(data);
    if(data.msg.length > 0) {
      this.setData({
      orderList: data.msg
    })
    }
  },

   /* 删除订单列表信息 */
   async delOrder(e) {
    let {data} = await fetch(`apis/del_orders`, {id: e.target.dataset.id}, 'get', {token: wx.getStorageSync('userInfo').token})
    // console.log(data);
    if(data.msg > 0) {
      wx.navigateTo({
        url: '/pages/user/order/order',
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getOrder(options.id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})