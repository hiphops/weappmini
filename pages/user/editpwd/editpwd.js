// pages/user/editpwd/editpwd.js
const fetch = require('../../../utils/fetch')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    oldpwd: '', // 旧密码
    upwd: '', // 新密码
    erruname: '' // 错误提示信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
// 检测旧密码不能为空
onuname(e) {
  if(this.data.oldpwd == '') {
    this.setData({erruname: '旧密码不能为空'})
    console.log(e);
  } else {
    this.setData({erruname: ''})
  }
},

/* 修改密码 */
async editpwd() {
  let {data} = await fetch(`user/resetpwd2`, {oldpassword: this.data.oldpwd, newpassword: this.data.upwd},'get', {})
  console.log(data);
  if(data.msg == '重置密码成功') {
    // wx.setStorageSync('userInfo', data.data.userinfo)
    wx.switchTab({
      url: '../../profile/profile' // 登录成功跳转到个人中心
    })
  } else {
     wx.showToast({
        title: `修改失败，${data.msg}`,
        icon: 'none',
      });
  }
},


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})