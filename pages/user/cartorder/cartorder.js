// pages/user/cartorder/cartorder.js
const fetch = require('../../../utils/fetch')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseurl: app.config.baseUrl, // 图片公共路径
    cartList: [], // 购物车列表
    total: 0, // 总计
    userInfo: {}, // 选中地址信息
    selectId: [], // 提交订单时保存购物车id
  },

  /* 判断结算时是否数据异常 */
  jiesuan() {
    if(wx.getStorageSync('orderData')) {
      this.setData({
        cartList: wx.getStorageSync('orderData')
      })
      // 统计总数
      var sum = 0
      var arr = []
      this.data.cartList.forEach(item => {
        sum += item.count * item.price
        arr.push(item.id)
      })
      this.setData({ total: sum, selectId: arr })
    } else {
      wx.showToast({
        title: `数据异常`,
        icon: 'none',
      });
      setTimeout(function() {
        wx.switchTab({
          url: '/pages/cart/cart',
        })
      }, 2000)
    }
  },

  /* 获取默认地址 */
  async getAddress() {
    let { data } = await fetch(`apis/getaddressmr`, { uid: wx.getStorageSync('userInfo').id }, 'get', {token: wx.getStorageSync('userInfo').token})
    console.log(data);
    if(data.msg.length > 0) {
      this.setData({
        userInfo: data.msg[0]
      })
    } else {
      wx.showToast({
        title: `获取地址失败`,
        icon: 'none',
      });
    }
  },

  /* 跳转到收货地址列表页 */
  toAddress() {
    wx.navigateTo({
      url: '/pages/user/address/address?txt=order',
    })
  },

  /* 提交订单 */
  async addOrder() {
    if(this.data.cartList[0].id == 0) {
      // console.log('ones', wx.getStorageSync('orderData'));
      // 立即购买
      let {data} = await fetch(`apis/save_order_ones`, { 
        uid: wx.getStorageSync('userInfo').id, total: this.data.total, 
        iconurl: wx.getStorageSync('orderData')[0].iconurl, 
        iconname: wx.getStorageSync('orderData')[0].iconname, 
        pname: wx.getStorageSync('orderData')[0].pname, count: 1, 
        price: wx.getStorageSync('orderData')[0].price, photo: wx.getStorageSync('orderData')[0].photo, person:  this.data.userInfo.person, 
        tel: this.data.userInfo.tel, 
        detail: this.data.userInfo.pro + ' ' + this.data.userInfo.city + ' ' + this.data.userInfo.area + ' ' + this.data.userInfo.detail
      }, 'get', {token: wx.getStorageSync('userInfo').token})
      if(data.msg > 0) {
        wx.removeStorageSync('orderData')
        wx.navigateTo({
          url: '/pages/user/order/order',
        })
      } else {
        wx.showToast({
          title: `生成订单失败，请重新操作`,
          icon: 'none',
        });
      }
    } else {
      // 从购物车购买
      let {data} = await fetch(`apis/save_order_alls`, { 
        uid: wx.getStorageSync('userInfo').id, total: this.data.total, 
        cids: this.data.selectId.join(','),
        person:  this.data.userInfo.person, 
        tel: this.data.userInfo.tel, 
        detail: this.data.userInfo.pro + ' ' + this.data.userInfo.city + ' ' + this.data.userInfo.area + ' ' + this.data.userInfo.detail
      }, 'get', {token: wx.getStorageSync('userInfo').token})
      if(data.msg > 0) {
        wx.removeStorageSync('orderData')
        wx.navigateTo({
          url: '/pages/user/order/order',
        })
      } else {
        wx.showToast({
          title: `生成订单失败，请重新操作`,
          icon: 'none',
        });
      }
    }
    

    // 订单提交成功后删除购物车的对应的数据，并清空本地存储列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    if(!wx.getStorageSync('userInfo')) {
        wx.redirectTo({
          url: '/pages/user/login/login'//页面路径
        })
      }
    // this.setData({ total: options.total })
    this.jiesuan()
    if(options.ad) {
      // console.log(options.ad);
      this.setData({
        userInfo: JSON.parse(options.ad)
      })
    } else {
      this.getAddress()
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.jiesuan()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})