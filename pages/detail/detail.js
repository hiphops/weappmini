const fetch = require('../../utils/fetch')
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    shop: {},
    baseurl: app.config.baseUrl,
    activeIndex1:0,
    activeIndex2:0,
    result: '', // 内容详情解析结果
  },

  /* 点击首页和购物车跳转 */
  toHome() {
    wx.switchTab({
      url: '../index/index',
    })
  },
  toCart() {
    wx.switchTab({
      url: '../cart/cart',
    })
  },



  /**
   * 生命周期函数--监听页面加载${options.item}
   */
  onLoad (options) {
    fetch(`apis/yhpro_find`, {id: options.item})
      .then(res => {
        console.log('aa',res);
        let result = app.towxml(res.data.msg.content,'markdown',{
          // base:'https://xxx.com',				// 相对资源的base路径
          theme:'dark',					// 主题，默认`light`
          events:{					// 为元素绑定的事件方法
            tap:(e)=>{
              console.log('tap',e);
            }
          }
        });
        this.setData({ shop: res.data.msg, result: result })
        // res.data.msg.icon_path.forEach(item => {
        //   console.log(item);
        // })
        wx.setNavigationBarTitle({ title: res.data.msg.name_ch })
      })
  },

  previewHandle (e) {
    wx.previewImage({
      current: e.target.dataset.src,
      urls: this.data.shop.images
    })
  },
  getType1(e) {
    console.log(e.target.dataset);
    this.setData({
      activeIndex1: e.target.dataset.index
    })
  },

  getType2(e) {
    console.log(e.target.dataset);
    this.setData({
      activeIndex2: e.target.dataset.index
    })
  },

    /* 添加到购物车 */
    async addCart() {
      // 用户没有登录时直接跳转到登录页面
      if(!wx.getStorageSync('userInfo')) {
        wx.redirectTo({
          url: '/pages/user/login/login'//页面路径
        })
      }
      var uid = wx.getStorageSync('userInfo').id // 用户id
      var pid = this.data.shop.id // 产品id
      var pname = this.data.shop.name_ch // 产品名
      var iconid = this.data.shop.icon_path[this.data.activeIndex1].id // 类型id
      var iconurl = this.data.shop.icon_path[this.data.activeIndex1].image // 类型图片
      var iconname = this.data.shop.icon_path[this.data.activeIndex1].name // 类型名称
      var price = this.data.shop.price // 产品价格
      var count = 1 // 产品数量
      var photo = this.data.shop.photo_path[this.data.activeIndex2].name // 选中规格名称
      let {data} = await fetch(`apis/save_carts`, {
        uid,
        pid,
        pname,
        iconid,
        iconurl,
        iconname,
        price,
        count,
        photo
      }, 'get', {token: wx.getStorageSync('userInfo').token})
      console.log('加入购车',data);
      if(data.msg == 1) {
        wx.showToast({
          title: `加入购物车成功`,
          icon: 'success',
        });
      } else {
        wx.showToast({
          title: `加入购物车失败`,
          icon: 'none',
        });
      }
    },
    
    /* 立即购买:把选中的信息传递到结算页面再提交订单 */
    goBuy() {
      var cartarr = []
      cartarr[0] = {
        id: 0, // 在结算页面判断是否是立即购买
        count: 1,
        iconid: this.data.shop.icon_path[this.data.activeIndex1].id,
        iconurl: this.data.shop.icon_path[this.data.activeIndex1].image, // 类型图片
        iconname: this.data.shop.icon_path[this.data.activeIndex1].name, // 类型名称
        photo: this.data.shop.photo_path[this.data.activeIndex2].name, // 选中规格名称
        pid: this.data.shop.id, // 产品id
        pname: this.data.shop.name_ch, // 产品名
        price: this.data.shop.price, // 产品价格
        uid: wx.getStorageSync('userInfo').id // 用户id
      }
      wx.removeStorageSync('orderData') // 先清除再存储
      wx.setStorageSync('orderData', cartarr)
      wx.navigateTo({
        url: '/pages/user/cartorder/cartorder',
      })
    }
})
