// pages/user/cart/cart.js
const fetch = require('../../utils/fetch')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseurl: app.config.baseUrl, // 图片公共路径
    cartList: [], // 购物车列表
    chkall: false, // 全选框的值
    total: 0, // 合计价格
    flagadd: false, // 数量增加按钮
    count: 0, // 选中总数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
     // 用户没有登录时直接跳转到登录页面
     if(!wx.getStorageSync('userInfo')) {
      wx.redirectTo({
        url: '/pages/user/login/login'//页面路径
      })
    } else {
        this.getCart()
    }
    
  },

  /* 获取用户的购物车列表 */
  async getCart() {
    let {data} = await fetch(`apis/cart_lists`, { uid: wx.getStorageSync('userInfo').id }, 'get', {token: wx.getStorageSync('userInfo').token})
    console.log('购物车列表',data);
    this.setData({
      cartList: data.msg.data
    })
    var flag = this.data.cartList.every(item => item.state == 1)
    this.setData({
      chkall: flag
    })
    // 调用总计价格
    this.getTotal()
  },

  /* 单个产品选择 */
  async chk(e) {
    console.log(e);
    var id = e.target.dataset.id
    var state = 0
    if(e.detail) {
      state = 1
    }
    let { data } = await fetch(`apis/cart_upstate`, { id, state }, 'get', {token: wx.getStorageSync('userInfo').token})
    console.log(data);
    if(data.msg == 1) {
      this.getCart()
    }
  },

  /* 全选框选择 */
  async setchkall(e) {
    var state = 0
    this.setData({ chkall: e.detail })
    if(e.detail) {
      state = 1
    }
    let { data } = await fetch(`apis/cart_upstateall`, { uid:wx.getStorageSync('userInfo').id, state }, 'get', {token: wx.getStorageSync('userInfo').token})
    console.log(data);
    if(data.msg > 0) {
      this.getCart()
    }
  },

  /* 获取选中产品的价格 和 选中产品数量 */
  getTotal() {
    // console.log(this.data.cartList);
    var sum = 0
    var num = 0
    this.data.cartList.forEach(item => {
      if(item.state == 1) {
        sum += item.price * item.count
        num += item.count
      }      
    })
    this.setData({
      total: sum,
      count: num
    })
  },

  /* 购物车修改数量 */
  async setNum(e) {
    // console.log(666);
    var id = e.target.dataset.id
    var count = e.detail
    // 点击时当数据没有修改完成则不允许再次点击
    this.setData({ flagadd: true }) // 禁用增加
    let { data } = await fetch(`apis/cart_upcounts`, { id, count }, 'get', {token: wx.getStorageSync('userInfo').token})
    console.log(data);
    if(data.msg > 0) {
      this.getCart()
      this.setData({ flagadd: false }) // 解除禁用增加
    }
  },

  /* 删除购物车 */
  async delCart(e) {
    var id = e.target.dataset.id
    let { data } = await fetch(`apis/cart_delete2`, { id}, 'get', {token: wx.getStorageSync('userInfo').token})
    if(data.msg > 0) {
      this.getCart()
    }
  },

  /* 结算 */
  onClickButton() {
    if(this.data.count == 0) {
      wx.showToast({
        title: `您还没选中商品`,
        icon: 'none',
      });
      return
    }
    var cartarr = this.data.cartList.filter(item => item.state == 1)
    wx.removeStorageSync('orderData') // 先清除再存储
    wx.setStorageSync('orderData', cartarr)
    wx.navigateTo({
      url: '/pages/user/cartorder/cartorder',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if(!wx.getStorageSync('userInfo')) {
        wx.redirectTo({
          url: '/pages/user/login/login'//页面路径
        })
      } else {
          this.getCart()
      }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})