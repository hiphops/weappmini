Page({
  /**
   * 页面的初始数据
   */
  data: {
    avatar: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad (options) {
    if(!wx.getStorageSync('userInfo')) {
      wx.redirectTo({
        url: '/pages/user/login/login'//页面路径
      })
    } else {
      this.setData({
        avatar: wx.getStorageSync('userInfo').avatar
      })
    }
  },

  /* 退出登录 */
  loginout() {
    wx.removeStorageSync('userInfo')
    wx.switchTab({
      url: '../index/index',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady () {
    console.log('aaa');
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow () {
    if(!wx.getStorageSync('userInfo')) {
      wx.redirectTo({
        url: '/pages/user/login/login'//页面路径
      })
    }else {
      this.setData({
        avatar: wx.getStorageSync('userInfo').avatar
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage () {

  }
})
